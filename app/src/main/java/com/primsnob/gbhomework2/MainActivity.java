package com.primsnob.gbhomework2;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.FileOutputStream;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = "MainActivity";

    @BindView(R.id.edit_text)
    EditText editText;
    @BindView(R.id.text_view)
    TextView textView;
    @BindView(R.id.receiver1_text_view)
    TextView receiver1TextView;
    @BindView(R.id.receiver2_text_view)
    TextView receiver2TextView;
    @BindView(R.id.subscribe_button)
    Button subscribeButton;
    @BindView(R.id.unsubscribe_button)
    Button unsubscribeButton;

    private static final int JPG_CHOOSER_REQUEST_CODE = 888;


    private PublishSubject<String> publishSubject = PublishSubject.create();
    private Disposable disposable1;
    private Disposable disposable2;
    private static final String PNG_FILE = "png_image";
    @BindView(R.id.convert_jpg_to_png_button)
    Button convertJpgToPngButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                publishSubject.onNext(String.valueOf(s));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        publishSubject.subscribe(textView::setText);

        EventBus.getDefault().register(this);

        Observable<Long> observable1 = Observable.interval(1, TimeUnit.SECONDS);
        Observable<Long> observable2 = Observable.interval(3, TimeUnit.SECONDS);

        subscribeButton.setOnClickListener(v -> {

            disposable1 = observable1.subscribe(
                    value -> EventBus.getDefault().post(new SomeEvent(value.toString()))
            );

            disposable2 = observable2.subscribe(
                    value -> EventBus.getDefault().post(new SomeEvent(value.toString()))
            );
        });

        unsubscribeButton.setOnClickListener(v -> {
            disposable1.dispose();
            disposable2.dispose();
        });


        convertJpgToPngButton.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/jpg");
            startActivityForResult(intent, JPG_CHOOSER_REQUEST_CODE);
        });

    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSomeEvent(SomeEvent event) {
        receiver1TextView.setText(receiver1TextView.getText().toString().
                concat(event.getMessage())
                .concat(" "));
        receiver2TextView.setText(event.getMessage());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == JPG_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri jpgFileUri = data.getData();

            Dialog dialog = new Dialog(this);
            View view = getLayoutInflater().from(this).inflate(R.layout.dialog, null);
            dialog.setContentView(view);
            Button dialogCancelButton = view.findViewById(R.id.dialog_cancel_button);

            Disposable d = Observable
                    .just(jpgFileUri)
                    .subscribeOn(Schedulers.computation())
                    .subscribe(
                            value -> {
                                Log.d(LOG_TAG, "Converting started");
                                Thread.sleep(1000);
                                Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(value));
                                Thread.sleep(1000);
                                FileOutputStream fileOutputStream = openFileOutput(PNG_FILE, MODE_PRIVATE);
                                Thread.sleep(1000);
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                            },
                            error -> {
                            },
                            () -> {
                                Log.d(LOG_TAG, "Converting complete");
                                dialog.cancel();
                            }
                    );

            dialogCancelButton.setOnClickListener(v -> {
                d.dispose();
                Log.d(LOG_TAG, "Converting canceled");
                dialog.cancel();
            });
            dialog.show();
        }
    }
}