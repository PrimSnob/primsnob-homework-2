package com.primsnob.gbhomework2;

public class SomeEvent {
    private String message;

    SomeEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
